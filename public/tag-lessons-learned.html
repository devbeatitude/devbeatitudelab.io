<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<link rel="alternate"
      type="application/rss+xml"
      href="https://devbeatitude.comrss.xml"
      title="RSS feed for https://devbeatitude.com"/>
<title>devbeatitude.com</title>
<meta name="author" content="David Miller">
<meta name="referrer" content="no-referrer">
<link href= "static/style.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="static/favicon.ico">
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8">
<meta name="viewport" content="initial-scale=1,width=device-width,minimum-scale=1"></head>
<body>
<div id="preamble" class="status"><div class="header">
  <a href="https://devbeatitude.com">devbeatitude.com</a>
  <div class="sitelinks">
    <a href="https://devbeatitude.com/rss.xml">RSS</a> | 
    <a href="https://gitlab.com/devbeatitude">GitLab</a> | 
    <a href="mailto:dmiller@davidocmiller.tech">Email Me</a>
  </div>
</div></div>
<div id="content">
<h1 class="title">Posts tagged "lessons-learned":</h1>
<div class="post-date">17 Oct 2018</div><h1 class="post-title"><a href="2018-10-18-how-not-to-manage-server-configs.html">How Not to Manage Server Configs</a></h1>
<p>
After working in software a few years, most people end up with a rich store of knowledge; unfortunately, this knowledge is mostly about how to do things wrong.  I know lots of ways to not manage a 200-person government software project, having been on several such beasts.
</p>

<p>
4 years into my current project (my longest tenure on a single project in my 30 years of work life), I know lots of ways to not manage a server farm.  In this case (unlike the huge government software projects) I am culpable; I setup the system myself, either directly, or through guidance and code reviews to my programmers.
</p>

<p>
My (broken) system is based on Git branches:
</p>

<ul class="org-ul">
<li><code>core</code>: default branch, should work out of the box on developer installations
<ul class="org-ul">
<li><code>foia</code>: branch for the <a href="https://www.foia.gov/">Freedom of Information Act</a> version of the prodct
<ul class="org-ul">
<li><code>foia-dev-server</code>: branch for the FOIA dev server</li>
<li><code>foia-test-server</code>: branch for the FOIA test server</li>
<li><code>foia-customer</code>: branch for one of our FOIA customers
<ul class="org-ul">
<li><code>foia-customer-dev-server</code>: branch for the customer dev server</li>
<li><code>foia-customer-test-server</code>: branch for the customer dev server</li>
</ul></li>
</ul></li>
<li><code>core-customer</code>: branch for one of our core customers
<ul class="org-ul">
<li><code>core-customer-dev-server</code>: branch for core customer dev server</li>
</ul></li>
</ul></li>
</ul>

<p>
And so on, and so on.
</p>

<p>
Commits to a branch flow to all downstream branches, so commits to <code>core</code> are applied to every branch; commits to <code>foia</code> are applied to the downstream <code>foia-*</code> branches; and so on.
</p>

<p>
Three facts make this a terrible system.  Fact 1: the repository includes binary files (spreadsheets).  Fact 2: my application updates many of these files at runtime.  Fact 3: there is no hierarchy of configuration values; each branch is a complete copy of the entire configuration.
</p>

<p>
You may already see the obvious problem; such a system can&rsquo;t be maintained automatically.  The all-too-frequent <a href="https://www.git-tower.com/learn/git/ebook/en/command-line/advanced-topics/merge-conflicts">merge conflicts</a> cause me to spend much of my time caring and feeding the dev, test, and production systems.  Merge conflicts in the spreadsheets are super-painful.
</p>

<p>
So what is to be done?  How can I fix it?
</p>

<p>
First, I have to establish a separation of responsibilities, by setting up a hierarchy of configuration values.  Default values (the <code>core</code> branch in my example Git branch tree) are maintained by the core application; values for the FOIA extension are maintained only by the FOIA version of the application; customer-specific values are maintained in only one place; and runtime changes are kept separate.  When the application needs a configuration value, it looks first for runtime values; then for server-specific values; then for extension values; then for core values&#x2026; Obviously this is done by a library so application code just looks up a key, like it does now.
</p>

<p>
Second, establish a uniform representation.  Everything is <a href="http://yaml.org/">YAML</a>; if it&rsquo;s not YAML, it&rsquo;s not configuration.  Replace our spreadsheets with YAML, then as the application starts, load the YAML data into the same structures as we load the spreadsheet into now.  Get rid of all the Spring configuration files; build the Spring beans from the YAML structure.
</p>

<p>
In this way we should get automated, conflict-free system administration.
</p>
<div class="taglist"><a href="tags.html">Tags:</a> <a href="tag-lessons-learned.html">lessons-learned</a> <a href="tag-cone-of-shame.html">cone-of-shame</a> </div>
<div class="post-date">16 Oct 2018</div><h1 class="post-title"><a href="2018-10-17-code-reviews-social-programming.html">Code Reviews Make for Social Programming</a></h1>
<p>
A long time ago on a project long dead, I setup a code review system.  At that time I was naive: the code review started <b>after</b> the code deployed to the development server.  If it seemed to work, the project manager was happy, and no one really cared about the code reviews.  So I switched our project from Subversion to Git, and setup a <a href="https://nvie.com/posts/a-successful-git-branching-model/">git-flow</a> model, where code lands in feature branches and must be reviewed before it gets deployed anywhere.<sup><a id="fnr.1" class="footref" href="#fn.1">1</a></sup>
</p>

<p>
As I am not a project manager or a software development manager or a scrum master, I have limited influence over how my programmers actually work, but I do exercise despotic control over the code itself.  This one change (enforcing code reviews) is the best thing I ever did in terms of code quality.
</p>

<p>
First, it lets everyone learn from each other.  I learned about Java 8 streams and lambdas from reviewing other people&rsquo;s code.  Some of the other guys have learned from my comments.
</p>

<p>
Second, sometimes we catch real issues before the code lands.  At least twice in the last two weeks, code reviews revealed a completely wrong path taken by the programmer, or a complete misunderstanding of the requirements.  Now, it would be really nice if we caught these issues even earlier, like <b>before</b> the programmer invests days going down the wrong path.  Still, better catch it in the code review, than after customer deployment.
</p>

<p>
Obviously the reviews are only as good as the reviewers. On another project in my company, &ldquo;code reviews&rdquo; consist of reviewers checking out the feature branch, running it locally, and seeing if it works&#x2026; so they are just duplicating the work of the system tester.  They are not reviewing for correctness, coding standards, security, efficiency, architectural adherence, etc.
</p>

<p>
Other times we review according to our history with the programmer.  I trust some of my programmers more than others.  This week, my most senior programmer sent me a short code review.  I reviewed it very quickly and accepted it.  During the build some unit tests failed!  The test failures were from really sloppy code this programmer ordinarily would never have turned in, and that I would never have accepted, if it came from a more junior guy that I didn&rsquo;t trust so much.
</p>

<p>
Good judgment in code reviews is a precious commodity.  A couple of my programmers are very good and thorough code reviewers.  If I had another couple reviewers like that, our product would be much better.
</p>
<div id="footnotes">
<h2 class="footnotes">Footnotes: </h2>
<div id="text-footnotes">

<div class="footdef"><sup><a id="fn.1" class="footnum" href="#fnr.1">1</a></sup> <div class="footpara"><p class="footpara">
Note that git-flow has its detractors: <a href="https://hackernoon.com/gitflow-is-a-poor-branching-model-hack-d46567a156e7">https://hackernoon.com/gitflow-is-a-poor-branching-model-hack-d46567a156e7</a>
</p></div></div>


</div>
</div><div class="taglist"><a href="tags.html">Tags:</a> <a href="tag-lessons-learned.html">lessons-learned</a> </div>
<div class="post-date">11 Oct 2018</div><h1 class="post-title"><a href="2018-10-11-know-the-platform.html">Know the Platform</a></h1>
<p>
<a href="https://devbeatitude.com/2018-10-11-local-optimals.html">Earlier today</a> I wrote about the pain of learning a new toolkit.  Afterward it occurred to me I never invested in IDEA or Eclipse, in the way I&rsquo;m being forced to invest in Emacs.  That made me think of all the other tools I use: <a href="https://spring.io">Spring</a>, <a href="https://en.wikipedia.org/wiki/Java_Persistence_API">JPA</a>, <a href="https://www.java.com/en/">Java</a>, <a href="https://junit.org/junit5/">JUnit</a>, <a href="https://en.wikipedia.org/wiki/Integration_testing">integration testing</a>&#x2026; I know a little about lots of things&#x2026; but only enough to get through my daily assignments.
</p>

<p>
Focusing on getting through my daily assignments, without a deep understanding of at least 3 - 4 of the really key tools, makes me a <a href="https://www.urbandictionary.com/define.php?term=corporate%20drone">corporate drone</a>, not a <a href="https://en.wikipedia.org/wiki/Software_engineer">professional software developer</a>.  
</p>

<p>
Is being a drone really bad?  I do value keeping a roof over my head, and of course my first loyalty is to my employer.  But, part of what my employer values me for is in fact a certain level of professionalism, more than I&rsquo;ve shown so far.
</p>

<p>
How to transition from drone to professional?
</p>

<p>
First: continue to keep my employer happy!
</p>

<p>
Second: learn:
</p>

<ul class="org-ul">
<li>Toolchain: Emacs, Java, Ansible</li>
<li>Platform: <a href="http://spring.io/projects/spring-boot">Spring Boot</a>, <a href="https://kubernetes.io/">Kubernetes</a></li>
<li>Practices: <a href="https://en.wikipedia.org/wiki/Software_requirements_specification">specification</a>, <a href="https://en.wikipedia.org/wiki/Software_testing">testing</a></li>
<li>Patterns: <a href="https://en.wikipedia.org/wiki/Domain-driven_design">Domain-driven design</a></li>
</ul>

<p>
Third: contribute.  Give back to at least one of the above communities&#x2026; contribute some code, <a href="https://meta.stackoverflow.com/questions/318295/how-do-i-contribute-to-stack-overflow">answer questions on Stack Overflow</a>, something!
</p>

<p>
Fourth: practice.  Start a <a href="https://dzone.com/articles/a-software-developers-guide-to-side-projects">side project</a>&#x2026; and finish it!
</p>

<p>
A lot to take on, to be sure.  I&rsquo;ll post back here on my progress.
</p>
<div class="taglist"><a href="tags.html">Tags:</a> <a href="tag-lessons-learned.html">lessons-learned</a> </div>
<div class="post-date">08 Oct 2018</div><h1 class="post-title"><a href="2018-10-09-samba-over-apacheds-over-openldap.html">Samba Over ApacheDS Over OpenLDAP</a></h1>
<p>
My application has internal users and groups, with role-based access control based on group membership&#x2026; in other words, it&rsquo;s a plain old corporate business application, not a retail app or a social network where anyone can sign in with Google, Facebook, Amazon etc.
</p>

<p>
So I need a <a href="https://en.wikipedia.org/wiki/Lightweight_Directory_Access_Protocol">directory server</a>. In the dawn of time (a few years ago) I gave my installation team more freedom to choose their own components&#x2026; Since our target platform is Linux, they chose <a href="https://www.openldap.org/">OpenLDAP</a>.  
</p>

<p>
OpenLDAP is a <b>major pain in the ass</b> to configure to our requirements.  I wrote ever so many lines of <a href="https://en.wikipedia.org/wiki/Bash_(Unix_shell)">shell script</a> to add a partition (e.g. <code>arkcase.com</code>); <a href="https://en.wikipedia.org/wiki/Transport_Layer_Security">enable TLS</a>; add three or four LDAP schema elements required by my application, but not loaded by default into OpenLDAP; and change the default admin password.
</p>

<p>
All this was on <a href="https://www.centos.org/">CentOS 6</a>.  Well, when CentOS 7 came out, all these scripts stopped working!  The CentOS 7 OpenLDAP packages broke something.  I didn&rsquo;t have enough guts to wade back into the code.
</p>

<p>
Where could we go from OpenLDAP?  Since my application is written in Java, next we chose <a href="https://directory.apache.org/apacheds/">ApacheDS</a>.  Still more oodles of shell script required!  In ApacheDS, the official documentation to add a partition says to use a GUI tool!  The only way to automate it is to use the tool once, record the <a href="https://en.wikipedia.org/wiki/LDAP_Data_Interchange_Format">LDIF</a> it generates, and write script code to generate the same LDIF.
</p>

<p>
And it didn&rsquo;t take long before we had real problems.  One of our teams had to load 5,000 test users; ApacheDS choked on the load script; too much traffic all at once.  Throw in random exceptions and occasional corrupted data stores, and we were ready to move on again.
</p>

<p>
Since pretty much all our customers are on <a href="https://en.wikipedia.org/wiki/Active_Directory">Active Directory</a>, we really wanted an Active Directory-compatible directory server that is free and runs on Linux.  Actually we had these three requirements (AD-compatible, free, Linux) this entire time, so the fact it took me so long to find <a href="https://wiki.samba.org/index.php/Setting_up_Samba_as_an_Active_Directory_Domain_Controller">Samba 4</a> just goes to show what a bad architect I am.
</p>

<p>
The <a href="https://wiki.samba.org/index.php/Setting_up_Samba_as_an_Active_Directory_Domain_Controller">guide</a> looks intimidating, but the process couldn&rsquo;t be simpler, especially if you only need Active Directory compatibility in terms of directory services (not so much DNS, file shares, Kerberos, certificate management&#x2026;).  All my oodles of shell script boil down to 150 lines of <a href="https://www.ansible.com/">Ansible</a> directives, most of which setup a folder structure.  After <code>configure ; make ; make install</code> I have a working Active Directory compatible server, complete with partition, TLS support, and my desired administrator password.
</p>

<p>
The lesson is simple, but I seem to forget it just as often as I learn it: <b>always go for the service that most closely meets your requirements</b>.  Samba is free; runs on Linux; compatible with Active Directory; and easy to install and configure.  
</p>
<div class="taglist"><a href="tags.html">Tags:</a> <a href="tag-ldap.html">ldap</a> <a href="tag-lessons-learned.html">lessons-learned</a> </div><div id="archive">
<a href="archive.html">Other posts</a>
</div>
</div>
</body>
</html>
